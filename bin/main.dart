// Copyright (c) 2016, Tomáš Rokos. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import "package:evernotebbissuetracker/evernotebbissuetracker.dart";
import "package:cloudbridges/requester_io.dart";

void main() {
    Tracker.sync(new RequesterIO(),
        "", // bb repo
        "",	// bitbucket authorization header value
        "", // evernote note id
        ""  // evernote token
    );
}
