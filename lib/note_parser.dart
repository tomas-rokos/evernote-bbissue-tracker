part of evernotebbissuetracker;

class NoteParser extends EvernoteNote {
	static List<Map> parseIssues(String noteXML) {
		NoteParser x = new NoteParser();
		x.parse(noteXML);

		Eksemel xn = x.children[x.noteChild];
		x.divRemoval(xn);
		x.removeSingleNewLines(xn);

		List<Map> result = x.parseLines(xn);
		return result;
	}

	void removeSingleNewLines(Eksemel e) {
		for (int i=0;i<e.children.length;++i) {
			Eksemel subE = e.children[i];
			if (subE.text == "\n") {
				e.children.removeAt(i);
				--i;
				continue;
			}
			removeSingleNewLines(subE);
		}

	}

	List<Map> parseLines(Eksemel e) {
		String currMilestone = "";
		String currComponenet = "";
		List<Map> result = [];
		for (int i=0;i<e.children.length;++i) {
			Eksemel subE = e.children[i];
			if (subE.tag == "" && subE.text.isNotEmpty) {
				Map mp = new Map();

				int idIdx = subE.text.indexOf("[");
				if (idIdx != -1) {
					int idIdxE = subE.text.indexOf("]",idIdx);
					int id = int.parse(subE.text.substring(idIdx+3,idIdxE-1));
					mp["title"] = subE.text.substring(idIdxE+1).trim();
					mp["id"] = id;
				} else {
					mp["title"] = subE.text;
				}

				mp["component"] = currComponenet;
				mp["milestone"] = currMilestone;
				result.add(mp);
			} else if (subE.tag == "b") {
				String t = subE.innerText.replaceAll("Milestone: ","");
				currMilestone = t == "Backlog" ? "" : t;
				currComponenet = "";
			} else if (subE.tag == "i") {
				currComponenet = subE.innerText;
			}
		}
		return result;
	}

}