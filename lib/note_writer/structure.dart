part of evernotebbissuetracker;

class NoteWriterStructure {
	List<Map> _issues;
	NoteWriterStructure(this._issues) {}

	List<String> milestones() {
		List<String> ret = [];
		_issues.forEach((Map issue) {
			String milestoneName = nestedName(issue,"milestone");
			if (ret.contains(milestoneName) == false)
				ret.add(milestoneName);
		});
		return ret;
	}
	List<String> components(String milestone) {
		List<String> ret = [];
		_issues.forEach((Map issue) {
			if (nestedName(issue,"milestone") != milestone)
				return;
			String componentName = nestedName(issue,"component");
			if (ret.contains(componentName) == false)
				ret.add(componentName);
		});
		return ret;
	}
	List<Map> issues(String milestone,String component) {
		List<Map> ret = [];
		_issues.forEach((Map issue) {
			if (milestone != null && nestedName(issue,"milestone") != milestone)
				return;
			if (component != null && nestedName(issue,"component") != component)
				return;
			ret.add(issue);
		});
		return ret;
	}
	static String nestedName(Map item, String key) {
		if (item[key] == null)
			return "";
		return item[key]["name"];
	}

}