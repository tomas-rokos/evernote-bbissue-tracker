part of evernotebbissuetracker;

class NoteWriter {

	static String write(List<Map> issues) {
		String note = "";
		bool firstMilestone = true;
		NoteWriterStructure structure = new NoteWriterStructure(issues);
		structure.milestones().forEach((String milestoneName) {
			if (firstMilestone) {
				firstMilestone = false;
			} else {
				note += "<div><hr /></div>\n";
			}
			if (milestoneName == "")
				note += "<div><b>Backlog</b></div>\n";
			else
				note += "<div><b>Milestone: "+milestoneName+"</b></div>\n";
			bool firstComponent = true;
			structure.components(milestoneName).forEach((String componentName) {
				if (firstComponent) {
					firstComponent = false;
				} else {
					note += "<div><br /></div>\n";
				}
				note += "<div><i>"+componentName+"</i></div>\n";
				structure.issues(milestoneName,componentName).forEach((Map issue) {
					note += "<div>";
					if (issue["state"] == "closed")
						note += "<s>";
					note += "[ #"+issue["id"].toString()+" ] "+issue["title"];
					if (issue["state"] == "closed")
						note += "</s>";
					note += "</div>\n";
				});
			});

		});
		return note;
	}
}