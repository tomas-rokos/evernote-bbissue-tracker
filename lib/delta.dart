part of evernotebbissuetracker;

class Delta {
	static List<Map> calculate(List<Map> base, List<Map> update) {
		List<Map> result = [];
		update.forEach((Map newItem) {
			if (newItem.containsKey("id") == false) {
				result.add(newItem);
				return;
			}
			base.forEach((Map oldItem) {
				if (oldItem["id"] != newItem["id"])
					return;
				Map deltaItem = {"id":oldItem["id"]};
				bool hasDiff = false;
				if (oldItem["title"] != newItem["title"]) {
					deltaItem["title"] = newItem["title"];
					hasDiff = true;
				}
				if (NoteWriterStructure.nestedName(oldItem,"milestone") != newItem["milestone"]) {
					deltaItem["milestone"] = newItem["milestone"];
					hasDiff = true;
				}
				if (NoteWriterStructure.nestedName(oldItem,"component") != newItem["component"]) {
					deltaItem["component"] = newItem["component"];
					hasDiff = true;
				}
				if (hasDiff)
					result.add(deltaItem);
			});
		});
		return result;
	}
}