library evernotebbissuetracker;

import "dart:async";
import "dart:convert";

import "package:cloudbridges/eksemel.dart";
import "package:cloudbridges/requester.dart";
import "package:cloudbridges/bitbucket_rest.dart";
import "package:cloudbridges/evernote_rest.dart";
import "package:cloudbridges/evernote_note.dart";

part "note_writer/note_writer.dart";
part "note_writer/structure.dart";
part "note_parser.dart";
part "delta.dart";

class Tracker {
    static Future sync(Requester rqs,String repo_name,String bb_auth,String noteid,String evntoken) async {
        BitbucketRest bbrest = new BitbucketRest(rqs,bb_auth);
        EvernoteRest evnrest = new EvernoteRest(rqs,evntoken);

        print("[X....] downloading note ...");
//        String noteOrig = await evnrest.getNote(noteid);
//        List<Map> issuesNote = NoteParser.parseIssues(noteOrig);

        print("[XX...] downloading issues ...");
        List issues = await bbrest.issues(repo_name);
//        List delta = Delta.calculate(issues,issuesNote);
//        print (JSON.encode(delta));

        print("[XXXXX] "+issues.length.toString()+" downloaded and now saving the result ...");
        String noteFinal = NoteWriter.write(issues);
//        print (noteFinal);
        String ntbs = await evnrest.setNote(noteid,noteFinal);
        print(ntbs);

    }
}