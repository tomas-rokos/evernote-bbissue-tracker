# Bitbucket issues tracker in Evernote

*This project is in "technology probe" state*

I would like to review issues inside Evernote application. In one note get a clear
picture about milestones, components and progress on issues.

## [Current video](https://youtu.be/He6RNqpRT5Y)


## Why Evernote?
[Evernote](http://www.evernote.com) is wonderful application for
taking/viewing notes. It's cloud based and has application for
majority of PC and Mobile systems.

The key motivation for me is offline view and existance of
 exteremely powerful UI. This means that I could focus only
 on data transfer.
 
## Current goal
View bitbucket issues structured inside one Evernote note. Initiating of 
snapshot synchronization is manual.

## Future goals
* Apply note text changes into corresponding issues.
* Create new issues based on new lines in note
 